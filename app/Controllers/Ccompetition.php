<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\M_competition;
use CodeIgniter\Exceptions\PageNotFoundException;

class Ccompetition extends Controller
{
	
	public function index()
	{
        $model = new M_competition();
        $data['result'] = $model->getAll();
		$data['pager'] = $model->pager;

		$data['page_title'] = "La liste des compétitions";
		$data['title'] = "La liste des compétitions";
       
       

		$page['contenu'] = view('Competitions/v_liste_competitions', $data);


		return view('Commun/v_template', $page);
	}

	public function detail($prmId = null){
		if($prmId != null){
			$model = new M_competition();
			$data['result'] = $model->getDetail($prmId);
			if(count($data['result']) != 0){
				$data['page_title'] = "Le classement ";
				$data['title'] = "La compétition ";

				$page['contenu'] = view('Competitions/v_detail_competition', $data);
				return view('Commun/v_template', $page);
			}else{
				throw PageNotFoundException::forPageNotFound("ce conteneur n'existe pas !");
			}
		}else{
			throw PageNotFoundException::forPageNotFound("il faut choisir un autre conteneur");
		}
	}
}
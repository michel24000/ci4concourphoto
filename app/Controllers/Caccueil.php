<?php

namespace App\Controllers;
use CodeIgniter\Controller;

class Caccueil extends Controller
{
	
	public function index()
	{
		//page d'accueil 
		$data['title'] = "Concours photographique";
		$data['page_title'] = "Concours photographique des TPCDA";

		$page['contenu'] = view('v_accueil', $data);


		return view('Commun/v_template', $page);
	}
}

<?php 

namespace App\Controllers\Api;

use CodeIgniter\Format\JSONFormatter;
use CodeIgniter\RESTful\ResourceController; 

class Competition extends ResourceController 
{ 
    protected $modelName = 'App\Models\M_competition'; // cet élément est récupéré grace à ResourceController et donne l'info à $model
    protected $model; 
    protected $format = 'json'; 
   
    public function index() 
    { 
        
        $result = $this->model->getAll(); 
        return $this->respond($result); 
    } 

    public function show($prmId = null)
    {
        $seg4 = $this->request->uri->getSegment(4);
        if ($seg4 != "photo") {
            var_dump($seg4);
            $result = $this->model->getDetailApi($prmId); 
            return $this->respond($result);
        } else {
            //Liste des photos de la compétition ID=$prmId
            $result = $this->model->getDetailApiPhoto($prmId); 
            return $this->respond($result);

            //restera à modifier l'url retourné
        }
    }
}
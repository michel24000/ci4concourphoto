<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Mphoto;
use CodeIgniter\Exceptions\PageNotFoundException;

class Cphoto extends Controller
{
	
	public function index()
	{
        
	}

    public function detail($prmId = null){
		if($prmId != null){
			$model = new Mphoto();
			$data['result'] = $model->getDetail($prmId);
			if(count($data['result']) != 0){
				$data['page_title'] = "";
				$data['title'] = "";

				$page['contenu'] = view('Photo/v_detail_photo', $data);
				return view('Commun/v_template', $page);
			}else{
				throw PageNotFoundException::forPageNotFound("ce conteneur n'existe pas !");
			}
		}else{
			throw PageNotFoundException::forPageNotFound("il faut choisir un autre conteneur");
		}
	}
}
<?php

namespace App\Models;

use CodeIgniter\Model;

class M_competition extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'competition';
	protected $primaryKey           = 'ID';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = [];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getAll(){
		$requete = $this->select('ID, Nom, DossierStockage, Date');
		return $requete->paginate(10);
	}

	public function getDetail($prmId){
		return $this->select('photo.Titre, photo.ID, Photo.Classement, concurrent.Pays, concurrent.Prenom, concurrent.Nom')
		->join('photo', 'photo.competitionID = competition.ID')
		->join('concurrent', 'photo.concurrentID = concurrent.ID')
		->orderBy('Photo.Classement')
		->where(['competition.ID' => $prmId])
		->findAll();
	}

	//getDetail() fonctionne bien sauf pour l'api me renvoi toutes les données comportant un 2, j'utilise donc celle ci-dessous pour l'api avec un paramétre.
	public function getDetailApi($prmId){
		return $this->select('ID, Nom, DossierStockage, Date')
					->where(['competition.ID' => $prmId])
					->findAll();
	}

	public function getDetailApiPhoto($prmId){
		return $this->select('photo.Classement, photo.Total as TotalPoints, competition.ID, photo.Titre, competition.Nom as Competition, concurrent.Nom as Auteur, concurrent.Prenom as Auteur, concurrent.Pays, photo.NomFichier')
					->join('photo', 'photo.competitionID = competition.ID')
					->join('concurrent', 'concurrent.ID = photo.concurrentID')
					->where(['competition.ID' => $prmId])
					->orderBy('photo.Classement')
					->findAll();
	}

}
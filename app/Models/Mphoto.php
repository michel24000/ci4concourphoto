<?php

namespace App\Models;

use CodeIgniter\Model;

class Mphoto extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'photo';
	protected $primaryKey           = 'ID';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = [];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getAll(){
		$requete = $this->select('Titre, NomFichier, OrdreProjection, Total, Classement');
		return $requete->paginate(10);
	}

	 public function getDetail($prmId){
	 	return $this->select('photo.Titre, Photo.Classement, photo.NomFichier, photo.Total, competition.Nom as cNom, concurrent.Pays, concurrent.Prenom, concurrent.Nom')
					->join('concurrent', 'photo.concurrentID = concurrent.ID', 'left')
					->join('competition', 'competition.ID = photo.competitionID', 'left')
					->where(['photo.ID' => $prmId])
					->findAll();
	 }

}